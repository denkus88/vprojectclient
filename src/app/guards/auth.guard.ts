import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MyStore } from '../types/store';

@Injectable()
export class AuthGuard implements CanActivate {
  userAuthorized$: Observable<boolean>;

  constructor(private store: Store<MyStore>, private router: Router) {
    this.userAuthorized$ = store.select('userAuthorized');
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.userAuthorized$.pipe(
      tap(authorized => !authorized ?
        this.router.navigate(['/login']) :
        null
      )
    );
  }
}
