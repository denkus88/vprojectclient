import { Component, OnInit } from '@angular/core';
import { TableColumnType, TableConfig } from '../../types/table';
import { TableDataService } from '../../services/table-data.service';
import { ProjectsTableDataService } from '../../services/projects-table-data.service';
import { Project } from '../../types/project';
import {Moment} from 'moment';

export const projectsTableConfigProvider: TableConfig = {
  columns: [
    {
      title: 'Id',
      type: TableColumnType.TEXT,
      colDef: 'id'
    },
    {
      title: 'Title',
      type: TableColumnType.TEXT,
      colDef: 'title'
    },
    {
      title: 'Type',
      type: TableColumnType.TEXT,
      colDef: 'type'
    },
    {
      title: 'Created at',
      type: TableColumnType.DATE,
      colDef: 'created'
    },
    {
      title: 'Modified at',
      type: TableColumnType.DATE,
      colDef: 'modified'
    },
    {
      title: 'Actions',
      type: TableColumnType.ACTIONS,
      colDef: 'actions'
    }
  ]

};

@Component({
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css'],
  providers: [
    { provide: 'tableConfig', useValue: projectsTableConfigProvider },
    { provide: TableDataService, useClass: ProjectsTableDataService }
  ]
})
export class ProjectsComponent implements OnInit {
  constructor(private projectsTableDataService: TableDataService<Project>) {}

  searchString: string;
  created: Moment;
  modified: Moment;
  isFavorite: boolean;

  ngOnInit(): void {
    (this.projectsTableDataService as ProjectsTableDataService)
      .queryParams
      .subscribe(searchParams => this.searchString = searchParams.searchString);
  }

  onSearch(): void {
    (this.projectsTableDataService as ProjectsTableDataService)
      .queryParams
      .next({ searchString: this.searchString, created: this.created, modified: this.modified, isFavorite: this.isFavorite });
  }
}
