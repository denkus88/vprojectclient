import { Component, Injectable, OnInit } from '@angular/core';
import { TableColumnType, TableConfig } from '../../types/table';
import { TableDataService } from '../../services/table-data.service';
import { UsersTableDataService } from '../../services/user-table-data.service';

export const userTableConfigProvider: TableConfig = {
  columns: [
    {
      title: 'Id',
      type: TableColumnType.TEXT,
      colDef: 'id'
    },
    {
      title: 'Email',
      type: TableColumnType.TEXT,
      colDef: 'email'
    },
    {
      title: 'Name',
      type: TableColumnType.TEXT,
      colDef: 'name'
    },
    {
      title: 'Projects',
      type: TableColumnType.TEXT_ARRAY,
      colDef: 'projects'
    },
    {
      title: 'Favorite Projects',
      type: TableColumnType.TEXT_ARRAY,
      colDef: 'favoriteProjects'
    },
    {
      title: 'Actions',
      type: TableColumnType.ACTIONS,
      colDef: 'actions'
    }
  ]

};

@Component({
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [
    { provide: 'tableConfig', useValue: userTableConfigProvider },
    { provide: TableDataService, useClass: UsersTableDataService }
  ]
})
export class UsersComponent implements OnInit {
  constructor() {
  }

  ngOnInit(): void {
  }

}
