import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { loginUser } from '../../actions/user.actions';
import { MyStore } from '../../types/store';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  constructor(private store: Store<MyStore>) {}

  onLogin(): void {
    this.store.dispatch(loginUser());
  }
}
