import { createReducer, on } from '@ngrx/store';
import { loginUser, userLogout } from '../actions/user.actions';

export const initialState = true;

const userAuthorizationReducer = createReducer(
  initialState,
  on(loginUser, state => true),
  on(userLogout, state => false)
);

export function userAuthReducer(state, action): boolean {
  return userAuthorizationReducer(state, action);
}
