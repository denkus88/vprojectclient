export type Project = {
  id: number;
  title: string;
  type: ProjectType;
  created: Date;
  modified: Date;
};
export enum ProjectType {
  NORMAL = 'NORMAL',
  URGENT = 'URGENT',
  SUSPENDED = 'SUSPENDED'
}
