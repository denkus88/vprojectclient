import { Project } from './project';
import { User } from './user';

export type DialogData = {
  title: string,
  values: Partial<User> | Partial<Project>
};
