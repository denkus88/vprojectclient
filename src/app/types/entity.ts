export enum EntityType {
  USER = 'user',
  PROJECT = 'project'
}
