export type User = {
  id: number;
  email: string;
  name: string;
  password: string;
  projects: UserProject[];
  favoriteProjects: UserProject[];
};

export type UserProject = { name: string, id: number };
