export type TableColumnConfig = {
  title: string,
  type: TableColumnType,
  colDef: string
};

export type TableConfig = {
  columns: TableColumnConfig[]
};

export enum TableColumnType {
  TEXT = 'text',
  ACTIONS = 'actions',
  TEXT_ARRAY = 'text_array',
  DATE = 'date'
}
