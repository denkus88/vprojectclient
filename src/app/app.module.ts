import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app.routing.module';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { userAuthReducer } from './reducers/user.reducer';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AuthGuard } from './guards/auth.guard';
import { UsersComponent } from './pages/users/users.component';
import { LoginComponent } from './pages/login/login.component';
import { ManagementComponent } from './pages/management/management.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { ProjectsComponent } from './pages/projects/projects.component';
import { MatIconModule } from '@angular/material/icon';
import { TableComponent } from './components/table/table.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { A11yModule } from '@angular/cdk/a11y';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { CreateEditDialogComponent } from './components/dialogs/create-edit-dialog/create-edit-dialog.component';
import { CreateButtonComponent } from './components/create-button/create-button.component';
import { MatSelectModule } from '@angular/material/select';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {BasicAuthInterceptor} from './intercepters/basic.auth.interceptor';

const vendorModules = [
  MatToolbarModule,
  MatButtonModule,
  MatTableModule,
  MatIconModule,
  MatInputModule,
  MatFormFieldModule,
  MatDialogModule,
  MatSelectModule,
  StoreModule.forRoot({ userAuthorized: userAuthReducer })
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ManagementComponent,
    UsersComponent,
    TableComponent,
    CreateEditDialogComponent,
    ProjectsComponent,
    BackButtonComponent,
    CreateButtonComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        FormsModule,
        A11yModule,
        AppRoutingModule,
        HttpClientModule,
        ...vendorModules,
        MatCheckboxModule
    ],
  providers: [AuthGuard , { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
