import { createAction } from '@ngrx/store';

export const loginUser = createAction('User log in');
export const userLogout = createAction('User log out');
