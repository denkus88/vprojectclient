import { Inject, Injectable } from '@angular/core';
import { TableColumnConfig, TableConfig } from '../types/table';

@Injectable()
export class TableConfigService {
  private config: TableConfig;

  constructor(@Inject('tableConfig') private tableConfig: TableConfig) {
    this.config = tableConfig;
  }

  get columnTitles(): string[] {
    return this.config.columns.map(column => column.title);
  }

  get columnIds(): string[] {
    return this.config.columns.map(column => column.colDef);
  }

  get columns(): TableColumnConfig[] {
    return this.config.columns;
  }
}
