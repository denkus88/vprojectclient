import {Injectable} from '@angular/core';
import {TableDataService} from './table-data.service';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Project} from '../types/project';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Moment} from 'moment';

interface SearchParams {
  searchString: string;
  created?: Moment;
  modified?: Moment;
  isFavorite?: boolean;
}

@Injectable()
export class ProjectsTableDataService extends TableDataService<Project> {
  readonly API_URI2 = `${environment.apiURL}/projects/search`;
  emailSearchParams = {searchString: ''};
  queryParams: BehaviorSubject<SearchParams> = new BehaviorSubject<SearchParams>(this.emailSearchParams);
  private readonly endpoint = `${environment.apiURL}/projects`;

  constructor(private http: HttpClient) {
    super();
  }

  getData(): Observable<Project[]> {
    return this.queryParams.pipe(
      switchMap(params => this.getDataByEmail(params).pipe(catchError(() => of([])))),
    );
  }

  getDataByEmail(searchParams): Observable<Project[]> {
    const URI = searchParams.searchString || searchParams.created || searchParams.modified || searchParams.isFavorite ? this.API_URI2 : this.endpoint;
    let params = new HttpParams();
    if (searchParams.searchString) {
      params = params.set('email', searchParams.searchString);
    }
    if (searchParams.created) {
      params = params.set('created', searchParams.created);
    }
    if (searchParams.modified) {
      params = params.set('modified', searchParams.modified);
    }
    if (searchParams.isFavorite) {
      params = params.set('isFavorite', searchParams.isFavorite);
    }
    return this.http.get<Project[]>(URI, {params});
  }

  postData(result: Project): Observable<Project[]> {
    return this.http.post<Project[]>(this.endpoint, result).pipe(tap(() => this.queryParams.next(this.emailSearchParams)));
  }

  deleteData(result: Project): Observable<void> {
    return this.http.delete<void>(`${this.endpoint}/${result.id}`).pipe(tap(() => this.queryParams.next(this.emailSearchParams)));
  }

  updateData(result: Project): void {
    this.http.put<Project[]>(this.endpoint, result)
      .pipe(
        catchError(() => of([])),
        tap(() => this.queryParams.next(this.emailSearchParams))
      ).subscribe();
  }

  getDataForUser() {
    return this.getData()
      .pipe(
        map(results => results.map(project => ({id: project.id, name: project.title}))
        )
      );
  }
}
