import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable()
export abstract class TableDataService<T> {

  abstract getData(): Observable<T[]>;

  abstract postData(result: T): Observable<T[]>;

  abstract deleteData(result: T): Observable<void>;

  abstract updateData(result: T): void;
}
