import {BehaviorSubject, Observable, of} from 'rxjs';
import {User} from '../types/user';
import {Injectable} from '@angular/core';
import {TableDataService} from './table-data.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {catchError, map, switchMap, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';

@Injectable()
export class UsersTableDataService extends TableDataService<User> {
  private readonly endpoint = `${environment.apiURL}/users`;
  queryParams: BehaviorSubject<string> = new BehaviorSubject<string>('');

  constructor(private http: HttpClient) {
    super();
  }

  getData(): Observable<User[]> {
    // return this.http.get<User[]>(this.API_URI, {});
    return this.queryParams.pipe(
      switchMap(params => this.getDataSubscription(params).pipe(catchError(() => of([])))),
    );
  }

  postData(result: User): Observable<User[]> {
    return this.http.post<User[]>(this.endpoint, result).pipe(tap(() => this.queryParams.next('')));
  }

  deleteData(result: User): Observable<void> {
    return this.http.delete<void>(`${this.endpoint}/${result.id}`).pipe(tap(() => this.queryParams.next('')));
  }

  updateData(result: User): void {
    this.http.put<User[]>(this.endpoint, result)
      .pipe(
        catchError(() => of([])),
        tap(() => this.queryParams.next(''))
      ).subscribe();
  }

  getDataSubscription(data): Observable<User[]> {
    let params;
    if (data) {
      params = new HttpParams().set('data', data);
      return this.http.get<User[]>(this.endpoint, {params});
    }
    return this.http.get<User[]>(this.endpoint, {});
  }

  getDataForUser() {
    return this.getData()
      .pipe(
        map(results => results.map(project => ({id: project.id, name: project.name}))
        )
      );
  }
}
