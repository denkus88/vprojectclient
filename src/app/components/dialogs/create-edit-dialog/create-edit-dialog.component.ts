import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {StringUtils} from '../../../utils/string.utils';
import {DialogData} from '../../../types/dialog';
import {MatSelectChange} from '@angular/material/select';
import {User, UserProject} from '../../../types/user';
import {UsersTableDataService} from '../../../services/user-table-data.service';
import {ProjectsTableDataService} from '../../../services/projects-table-data.service';

@Component({
  selector: 'app-create-edit-dialog',
  templateUrl: './create-edit-dialog.component.html',
  styleUrls: ['./create-edit-dialog.component.css'],
  providers: [UsersTableDataService, ProjectsTableDataService]
})
export class CreateEditDialogComponent implements OnInit {
  projectTypes = ['NORMAL', 'URGENT', 'SUSPENDED'];
  keys: string[];
  projectsList: UserProject[] = [];
  formInvalid = true;

  constructor(
    public dialogRef: MatDialogRef<CreateEditDialogComponent>,
    private usersTableDataService: UsersTableDataService,
    private projectsTableDataService: ProjectsTableDataService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {
    this.keys = Object.keys(this.data.values);
    // this.checkProjects();
  }

  ngOnInit(): void {
    this.projectsTableDataService.getDataForUser().subscribe(result => {
      this.projectsList = result;
      this.checkProjects();
    });
    this.checkRequiredFields();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  checkRequiredFields(): void {
    const inputKeys = this.keys.filter(key => key !== 'projects' && key !== 'favoriteProjects');
    this.formInvalid = inputKeys.map(key => Boolean(this.data.values[key])).includes(false);
  }

  convertKeyToTitle(key: string): string {
    return StringUtils.convertFromCamelCaseToSentence(key);
  }

  onProjectSelectionChange(changes: MatSelectChange): void {
    const {value} = changes;
    const dialogValues = (this.data.values as Partial<User>);

    dialogValues.favoriteProjects = (dialogValues.favoriteProjects || []).filter(proj => value.includes(proj));
  }

  private checkProjects() {
    if ((this.data.values as User).projects) {
      (this.data.values as User).projects = this.projectsList
        .filter(value => (this.data.values as User).projects
          .filter(proj => {
            return proj.id === value.id;
          })[0]);
    }
    if ((this.data.values as User).favoriteProjects) {
      (this.data.values as User).favoriteProjects = this.projectsList
        .filter(value => (this.data.values as User).favoriteProjects
          .filter(proj => {
            return proj.name === value.name;
          })[0]);
    }
  }
}
