import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.css']
})
export class BackButtonComponent {
  @Input() routerLink: string;

  constructor(private router: Router) {
  }

  onClick(): void {
    this.router.navigate([this.routerLink]);
  }
}
