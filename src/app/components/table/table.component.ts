import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import { TableConfigService } from '../../services/table-config.service';
import { TableColumnConfig } from '../../types/table';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditDialogComponent } from '../dialogs/create-edit-dialog/create-edit-dialog.component';
import { TableDataService } from '../../services/table-data.service';
import { Observable } from 'rxjs';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  providers: [TableConfigService]
})
export class TableComponent<T> implements OnInit {
  tableColumns: TableColumnConfig[];
  displayedColumns: string[];
  dataSource$: Observable<T[]>;

  constructor(
    private tableConfigService: TableConfigService,
    private tableDataService: TableDataService<T>,
    private dialog: MatDialog
  ) {
    this.tableColumns = tableConfigService.columns;
    this.displayedColumns = this.tableConfigService.columnIds;
  }

  ngOnInit() {
    this.dataSource$ = this.tableDataService.getData();
  }

  onEditClick(data: T): void {
    const dialogRef = this.dialog.open(
      CreateEditDialogComponent,
      { minWidth: '250px', minHeight: '250px', data : { title: 'Edit selected row', values: {...data} }}
    );

    dialogRef.afterClosed().subscribe(result => {
      if (result) { this.tableDataService.updateData(result); }
    });
  }

  onDeleteClick(data: T) {
    this.tableDataService.deleteData(data).subscribe();
  }
}

