import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {EntityType} from '../../types/entity';
import {User} from '../../types/user';
import {Project, ProjectType} from '../../types/project';
import {CreateEditDialogComponent} from '../dialogs/create-edit-dialog/create-edit-dialog.component';
import {TableDataService} from '../../services/table-data.service';

@Component({
  selector: 'app-create-button',
  templateUrl: './create-button.component.html',
  styleUrls: ['./create-button.component.css']
})
export class CreateButtonComponent {
  @Input() entityType: EntityType;

  constructor(private dialog: MatDialog,
              private tableDataService: TableDataService<User | Project>) {
  }

  onClick(): void {
    let data: Partial<User> | Partial<Project>;

    if (this.entityType === EntityType.USER) {
      data = {email: '', name: '', password: '', projects: []};
    } else if (this.entityType === EntityType.PROJECT) {
      data = {title: '', type: ProjectType.NORMAL};
    }

    const dialogRef = this.dialog.open(
      CreateEditDialogComponent,
      {minHeight: '250px', minWidth: '250px', data: {title: `Create new ${this.entityType}`, values: {...data}}}
    );

    dialogRef.afterClosed().subscribe(result => {
      this.tableDataService.postData(result).subscribe();
    });
  }


}
