export class StringUtils {
  static convertFromCamelCaseToSentence(str): string {
    const stringWithSpaces = str.replace(/([A-Z])/g, ' $1');
    return stringWithSpaces.slice(0, 1).toLocaleUpperCase() + stringWithSpaces.slice(1).toLocaleLowerCase();
  }
}
